using System;
using CG.ODP.Osoba;
using Xunit;

namespace ODP
{
    public class OsobaTests
    {
        [Fact]
        public void Osoba_Add_True()
        {
            OsobaManager om = new OsobaManager();
            Assert.True(om.AddOsoba("Jan", "Kowalski"));
        }

    }
}
