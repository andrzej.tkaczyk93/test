using System;
using CG.Lib.Configuration;
using CG.Lib.Logger;
using Xunit;

namespace Tests.CG.Logger
{
    public class LoggerTests : IDisposable
    {

        private CGLogger logger { get; set; }

        public LoggerTests()
            => logger = new CGLogger();

        public void Dispose()
            => logger = null;

        [Fact]
        public void Error_OnConsole_True()
        {
            logger.Error("Error test");
            Assert.True(logger.BylZapisDoKonsoli);
        }

        [Fact]
        public void Warning_OnConsole_True()
        {
            logger.Warning("Warning test");
            Assert.True(logger.BylZapisDoKonsoli);
        }

        [Fact]
        public void Info_OnConsole_False()
        {
            logger.Info("Info test");
            Assert.False(logger.BylZapisDoKonsoli);
        }

        [Fact]
        public void Debug_OnConsole_False()
        {
            logger.Debug("Debug test");
            Assert.False(logger.BylZapisDoKonsoli);
        }

    }

}