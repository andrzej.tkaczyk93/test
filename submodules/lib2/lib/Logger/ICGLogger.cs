﻿using Microsoft.Extensions.Logging;

namespace CG.Lib.Logger
{
    public interface ICGLogger : ILogger
    {
        void Error(string tekst);
        void Warning(string tekst);
        void Info(string tekst);
        void Debug(string tekst);
    }
}
