﻿using Microsoft.Extensions.Logging;

namespace CodeFirst.Lib.Logger
{
    public class CGLoggerProvider : ILoggerProvider
    {

        ILogger _logger = null;

        public CGLoggerProvider(ILogger logger)
            => _logger = logger;

        // Na razie nie interesuje nas podział loggera na tym poziomie.
        // Rozgraniczać będziemy na poziome pojedyńczego logowania w zależności od event-u.
        public ILogger CreateLogger(string categoryName)
            => _logger;

        public void Dispose() { }

    }
}
