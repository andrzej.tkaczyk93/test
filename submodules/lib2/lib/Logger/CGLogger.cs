﻿using CG.Lib.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace CG.Lib.Logger
{

    public class CGLogger : ICGLogger
    {

        #region Stałe i atrybuty.
        // Na potrzeby filtrowania logera Entity Framework.
        // TODO znależć dokumentacje do tych stałych.
        private readonly int STATEMENT_EVENT_ID = 20101;

        // Kompletny log.
        private readonly string FullLogFile;

        // Entity Framework statements log.
        private readonly string StatementsLogFile = Config.LoggerStatement;

        private readonly LogLevel LogLevel;

        private bool zapisDoKonsoli = false;
        public bool BylZapisDoKonsoli  { get { return zapisDoKonsoli; } }

        #endregion

        public CGLogger()
        {
            FullLogFile = Config.Logger;
            try {
                // Zamiana wartości tekstowej na typ poziomu logowania.
                LogLevel = (LogLevel)Enum.Parse(typeof(LogLevel), Config.LoggerLevel);
            } catch (Exception e) {
                Error(e.Message + Environment.NewLine + e.StackTrace);
                LogLevel = LogLevel.Warning;
            }
            if (StatementsLogFile == null)
                Debug("Brak podanego pliku logu komend SQL.");
        }

        public bool IsEnabled(LogLevel logLevel)
            => logLevel >= LogLevel;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (FullLogFile != null)
                File.AppendAllText(FullLogFile, formatter(state, exception));
            if (StatementsLogFile != null && eventId.Id == STATEMENT_EVENT_ID)
                File.AppendAllText(StatementsLogFile, formatter(state, exception) + "\n\n"); // Dodatkowa nowa linia dla przejrzystości.
        }

        public IDisposable BeginScope<TState>(TState state)
            => null;

        private void ZapiszDoPliku(string tekst)
        {
            if (FullLogFile != null)
              File.AppendAllText(FullLogFile, tekst + "\n");
        }

        private void ZarejestrujWLogu(string tekst, LogLevel poziom)
        {
            if (IsEnabled(poziom))
                ZapiszDoPliku(tekst);
        }

        private void ZarejestrujNaKonsoli(string tekst, LogLevel poziom)
        {
            if (IsEnabled(poziom)) {
                zapisDoKonsoli = true;
                Console.Error.WriteLine(tekst);
            }
        }

        public void Error(string tekst)
        {
            ZarejestrujNaKonsoli("Error: " + tekst, LogLevel.Error);
            ZarejestrujWLogu(tekst, LogLevel.Error);
        }

        public void Warning(string tekst)
        {
            ZarejestrujNaKonsoli("Warning: " + tekst, LogLevel.Error);
            ZarejestrujWLogu(tekst, LogLevel.Warning);
        }

        public void Info(string tekst)
            => ZarejestrujWLogu(tekst, LogLevel.Information);

        public void Debug(string tekst)
            => ZarejestrujWLogu(tekst, LogLevel.Debug);
    }

}