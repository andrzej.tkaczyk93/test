﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace CG.Lib.Configuration
{
    public class Config
    {

        #region Stałe.
        private static readonly string CG_CONFIG_FILE = "appsettings.json";
        private static readonly string MS_CONFIG_FILE = "mssettings.json";
        private static readonly string DEFAULT_LOGGER = "log.txt";
        private static readonly string DEFAULT_STATEMENST_LOGGER = "log_statements.txt";
        private static readonly string DEFAULT_LOGGER_LEVEL = "Warning";
        #endregion

        // Ustawienia globalne dla CG.
        public static readonly IConfigurationRoot CGSettings = getSettings(CG_CONFIG_FILE);
        public static string Logger = getCGProperty("Logging:LogFile:Full", DEFAULT_LOGGER);
        public static string LoggerStatement = getCGProperty("Logging:LogFile:Statements", DEFAULT_STATEMENST_LOGGER);
        public static string LoggerLevel = getCGProperty("Logging:LogLevel:Default", DEFAULT_LOGGER_LEVEL);

        // Ustawienia lokalne dla MS.
        public static readonly IConfigurationRoot MSSettings = getSettings(MS_CONFIG_FILE);
        public static string PostgresConnectionString = MSSettings?["PostgresConnectionString"];

        private static IConfigurationRoot getSettings(string configFile)
        {
            try
            {
                if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), configFile)))
                {
                    return new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile(configFile).Build();
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
                return null;
            }
        }

        private static string getCGProperty(string path, string defaultValue) 
            => (CGSettings != null) ? CGSettings[path] : defaultValue;

    }
}
