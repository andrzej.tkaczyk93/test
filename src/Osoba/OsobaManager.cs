using CG.Lib.Logger;

namespace CG.ODP.Osoba
{
    public class OsobaManager
    {

        ICGLogger log = new CGLogger();

        public bool AddOsoba(string Nazwisko, string imię)
        {
            log.Warning("Dodano osobę.");
            return true;
        }

        public bool RemoveOsoba(string Nazwisko, string imię)
        {
            log.Warning("Usunięto osobę.");
            return true;
        }

    }

}